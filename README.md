
###OVERVIEW
this is a 2D coop side scroller, the target platform is Windows and Mac, control is Keyboard + mouse, and GamePad.  Inspired by classic games like Sunset Riders.  
Note: All Graphics are placeholder and temporary.  

[WATCH DEMO](https://www.youtube.com/watch?v=SdvlL0kd4pQ)

![Pardeep Chahal](http://pardeepchahal.com/wp-content/uploads/2018/01/main.png)

###CONTROLS
   The player can can move in 8 directions, depending on the current state.  
Possible actions include Walking, sliding, climbing stairs and ladders, sliding, climbing ropes, 

###ARCHITECTURE
Character class  base class for types of actors
Player  subclass of Character the guy you control
Enemy  subclass of Character all the npc in the game such as brute, sniper,dog, guard �
All characters have a scriptable object property that stores basic attributes, this allows easy tweaking properties and ability to save changes in editor mode    

Since unity will not serialize auto properties, I am using properties with backing fields, so we view them in the UnityEditor inspector.

EventsManager - handles game events like death, health change, etc

ComponentBased - Input and Physics are defined as Interfaces, this will allow us to for example change the Input to come from a file or touch if we need to.  See
IInputComponent, IPhysicsComponent

    [System.Serializable]
    public class PlayerScriptableObject:ScriptableObject
    {
    [SerializeField]
    public int Health = 1;
    [SerializeField]
    public  float Speed = 5;
    [SerializeField]
    public float reloadTime = 1;
    [SerializeField]
    public float fireRate = .3f;
    [SerializeField]
    public float lineOfSight = 7;
    [SerializeField]
    public float bulletForce = 25;
    [SerializeField]
    public int bulletCount = 10;
    [SerializeField]
    public int bulletCapacity = 10;
    }

###GAME WORLD
The game world is tile based, and set on a 1unit = 1metre scale.   Players are 2units~ 2metres.
We can quickly build and prototype levels using the TileMap editor extension available here
[Tile Editor](https://github.com/pchahal/TileMapEditor).  We can use the tile Editor to place all tiles including floors, walls, windows, interactive items such as barrels, as well as enemy placement.  We can also quickly setup nodes for each ai path. 
 
**Stairs**
both enemies and players can use ladders and stairs.   The player is is firing raycasts in the direction he is facing every frame  to check for stairs.  Only if the player is pushing up on stick and facing the stair will he climb up.  For enemies they will climb stairs if the next node in their path is higher vertical position,  this check is done every frame as well for enemies that are patrolling.   The trick is to detect stairs and if the player or enemy want to ascend we turn on the stair collision, otherwise it is off.

**Ladders**
work similar to stairs, enemies and the player can climb up and down.  If they player detects a stair when firing a recast in his forward direction and if he is pressing up or down on the stick, he will climb the stairs.  Similarly if the enemy detects a stair and the next node in their A* path is higher or lower than the current node, they will use the ladder.

**Ropes**
only player can climb ropes, player can go from Jumping -> Rope Climbing, 

**Cover**
a cover spot allows the player to slide into cover if it is close by, in this position he is immune to Damage.   When a player detects a cover spot and moves in, bullets will not affect his health.
Enemies like stealth enemies also pop in and out of cover.  Enemy can pop into cover reload then fire again,  during cover the player can not shoot the enemy.  


###AI
**A* Pathfinding**
for some types of enemies a* pathfinding is used.  
Certain enemies like brutes, patrol, dog, use A* pathfinding to traverse the level.   This game uses the A* pathfinding project package from arongranberg.com.    

**State Machine**
enemies can be in many different states such as idle, patrolling, shooting, inCover   
![Pardeep Chahal](http://pardeepchahal.com/wp-content/uploads/2018/01/StandingState.png)
Each state has a corresponding Statemachinebehaviour, mecanim will call those scripts.  For instance in the EnemyClimbingState, OnStateUpdate()  will be called every frame in this state allowing us to check if the enemy is still on the ladder.
The player character has a much more complicated mechanism animator setup and also uses a state machine for controlling animations and behaviour with the StateMachineBehaviours.


![Pardeep Chahal](http://pardeepchahal.com/wp-content/uploads/2018/01/PlayerState.png)
The player can do many actions from climbing ladders, moving up and down stairs, shimmy on Ropes, sliding, ducking , etc..
Many states like walking, standing, jumping have sub states to handle . For instance when the player is Standing, only here can he be able to duck.  All animations and behaviour specific to player Standing will happen in this state, using the StandingState machine behaviour script.



###ANIMATIONS
**Mecanim**
is driving all the Player and Enemy animations. Currently animations are done with sprite sheets but this could change if we want to move to 2D skeletal animation.  
**DoTween** 
for simple tweens for things like simple position and rotation animations.

