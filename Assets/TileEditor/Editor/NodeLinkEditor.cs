﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using Pathfinding;

[CustomEditor(typeof(NodeLink))]
public class NodeLinkEditor : Editor
{
    float sphereSize = .2f;
    NodeLink nodeLink;

    void OnSceneGUI()
    {
        nodeLink = (NodeLink)target;

        Handles.color = Color.red;
       

        Handles.SphereCap(0, nodeLink.transform.position, Quaternion.identity, sphereSize);
        Handles.SphereCap(0, nodeLink.transform.GetChild(0).transform.position, Quaternion.identity, sphereSize);

        nodeLink.transform.position = Handles.FreeMoveHandle(nodeLink.transform.position, Quaternion.identity, .5f, Vector3.one, Handles.RectangleCap);

    
        nodeLink.transform.GetChild(0).transform.position = Handles.FreeMoveHandle(nodeLink.transform.GetChild(0).transform.position, Quaternion.identity, .5f, Vector3.one, Handles.RectangleCap);






    }
}
