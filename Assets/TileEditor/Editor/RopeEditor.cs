﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(Rope))]
public class RopeEditor : Editor
{
    float sphereSize = .3f;
    Rope rope;

    void OnSceneGUI()
    {
        rope = (Rope)target;

        Handles.color = Color.red;
        Handles.SphereCap(0, rope.startPoint.transform.position, rope.transform.rotation, sphereSize);
        Handles.SphereCap(0, rope.endPoint.transform.position, rope.transform.rotation, sphereSize);

        rope.transform.parent.position = Handles.FreeMoveHandle(rope.startPoint.transform.position, Quaternion.identity, .5f, Vector3.one, Handles.RectangleCap);
        rope.endPoint.transform.position = Handles.FreeMoveHandle(rope.endPoint.transform.position, Quaternion.identity, .5f, Vector3.one, Handles.RectangleCap);
    

        float distance = Vector3.Distance(rope.startPoint.transform.position, rope.endPoint.transform.position);

        Vector3 ropeDir = rope.endPoint.transform.position - rope.startPoint.transform.position;
        float angle = Vector2.Angle(Vector3.right, ropeDir);
        BoxCollider2D collider = rope.GetComponent<BoxCollider2D>();

        collider.size = new Vector2(distance, collider.size.y);
        collider.offset = new Vector2(distance / 2, 0);

        collider.transform.rotation = Quaternion.Euler(0, 0, angle * Mathf.Sign(ropeDir.y));


        rope.UpdateRopePoints();

    }
}
