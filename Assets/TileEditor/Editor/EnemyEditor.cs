﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using Pathfinding;
using System.Collections.Generic;

[CustomEditor(typeof(Enemy))]
public class EnemyEditor : Editor
{
    float sphereSize = .2f;
    Enemy enemy;

  

    void OnEnable()
    {
        enemy = (Enemy)target;

       
                   

    }


    void OnSceneGUI()
    {       
        Handles.color = Color.red;
       
        for (int i = 0; i < enemy.WayPoints.Count; i++)
        {           
            Handles.SphereCap(0, enemy.WayPoints[i].position, Quaternion.identity, sphereSize);    
            enemy.WayPoints[i].position = Handles.FreeMoveHandle(enemy.WayPoints[i].position, Quaternion.identity, .5f, Vector3.one, Handles.RectangleCap);
        }

    }
}
