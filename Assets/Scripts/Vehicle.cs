﻿using UnityEngine;
using System.Collections;
using SunsetRiders;

public class Vehicle : MonoBehaviour
{
    public GameObject smokePrefab;
    public GameObject seat;
    public SpriteRenderer frontWheel;
    public SpriteRenderer rearWheel;
    public SpriteRenderer body;
    public SmoothCamera2D camera;
    public Transform player;

    // Use this for initialization
    void Start()
    {
	
       
    }
	
    // Update is called once per frame
    void Update()
    {
	
    }


    void OnCollisionEnter2D(Collision2D collision)
    {
      
        AudioManager.Instance.PlayDebris();
        WheelJoint2D[] joints = GetComponentsInParent<WheelJoint2D>();
        foreach (var joint in joints)
        {
            joint.useMotor = false;
        }

        smokePrefab.SetActive(true);
        EventManager.Instance.PostNotification(EVENT_TYPE.CAMSHAKE, null, null);
        EventManager.Instance.PostNotification(EVENT_TYPE.DEAD, null, null);
        Invoke("Destroy", 5);
        Destroy(GetComponent<BoxCollider2D>());
        Destroy(GetComponent<PolygonCollider2D>());
       

     

    }


    void Destroy()
    {
        rearWheel.sortingOrder = 1;
        frontWheel.sortingOrder = 1;
        body.sortingOrder = 1;
        Destroy(seat);
        camera.target = player;
        camera.dampTime = .5f;
        camera.targetOffset = new Vector3(5, 4, 0);
      
    }
}
