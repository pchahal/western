﻿using UnityEngine;
using System.Collections;

public class Door : MonoBehaviour
{
    Animator anim;
    AudioSource audio;
    // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animator>();
        audio = GetComponent<AudioSource>();
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        
        if (collider.tag == "Player" || collider.tag == "Enemy")
        {
          
            if (collider.transform.position.x > transform.position.x)
                GetComponent<SpriteRenderer>().flipX = true;
            else
                GetComponent<SpriteRenderer>().flipX = false;
            anim.SetBool("Open", true);
            audio.Play();
        }
        Destroy(GetComponent<BoxCollider2D>());
    }

    void OnTriggerExit2D(Collider2D collider)
    {
        //   if (collider.tag == "Player" || collider.tag == "Enemy")
        //     Invoke("CloseDoor", 2);
    }

    void CloseDoor()
    {        
        anim.SetBool("Open", false);
        audio.Play();

    }
}
