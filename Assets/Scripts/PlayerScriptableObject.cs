﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class PlayerScriptableObject:ScriptableObject
{
    [SerializeField]
    public int Health = 1;
    [SerializeField]
    public  float Speed = 5;
    [SerializeField]
    public float reloadTime = 1;
    [SerializeField]
    public float fireRate = .3f;
    [SerializeField]
    public float lineOfSight = 7;
    [SerializeField]
    public float bulletForce = 25;
    [SerializeField]
    public int bulletCount = 10;
    [SerializeField]
    public int bulletCapacity = 10;
	
}

