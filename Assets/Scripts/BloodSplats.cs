﻿using UnityEngine;
using System.Collections;
using SunsetRiders;

public class BloodSplats : MonoBehaviour
{
    public GameObject[] bloodPrefabs;


    void Start()
    {
        EventManager.Instance.AddListener(EVENT_TYPE.ENEMY_DEAD, OnEvent);


    }

    public void OnEvent(EVENT_TYPE Event_Type, Component Sender, object Param = null)
    {
        //Detect event type
        switch (Event_Type)
        {
            case EVENT_TYPE.ENEMY_DEAD:
                Debug.Log("Param");
                OnPlayerDied((Vector3)Param);
                break;
         

        }
    }


    void OnPlayerDied(Vector3 Position)
    {
        
        Debug.Log(Position);

        GameObject blood = Instantiate(bloodPrefabs[Random.Range(0, bloodPrefabs.Length)], Position, Quaternion.identity) as GameObject;
        blood.transform.parent = transform;

    }

    // Update is called once per frame
    void Update()
    {
	    
    }


}
