using UnityEngine;
using System.Collections;
using SunsetRiders;

public class Bullet : MonoBehaviour
{
    public Transform spark;
    public float speed = 5;
    public Vector3 direction;
    private float startTime;

    void Start()
    {
        startTime = Time.time;
    }


    void FixedUpdate()
    {
        
        Vector3 nextPos = transform.position + direction * speed * Time.deltaTime;
        RaycastHit2D[] hits = Physics2D.LinecastAll(transform.position, nextPos);
        Debug.DrawLine(transform.position, nextPos, Color.cyan);
        foreach (var hit in hits)
        {
            if (hit.collider != null)
            {
                OnBulletEnter(hit.collider);
                
            }
        
        }

        transform.position = nextPos;
       

    }

    public void OnBulletEnter(Collider2D collider)
    {


          
        if (collider.gameObject.layer == gameObject.layer || collider is CircleCollider2D)
            return;


        if (collider.tag == "Player" || collider.tag == "Enemy")
        {                     
            Character enemy = collider.gameObject.GetComponent<Character>() as Character;
            bool didHit = enemy.OnHit(Vector3.zero, direction, 1);
            if (didHit)
                Destroy(gameObject);
        }
        else if (collider.tag == "Explosive")
        {
            IHittable explosive = collider.gameObject.GetComponent<IHittable>() as IHittable;
            explosive.OnHit(Vector3.zero, direction, 1);
            Destroy(gameObject);

        }
        else if (collider.tag == "Ground" || collider.tag == "Wall")
        {
            GameObject sparkGO = Instantiate(spark, transform.position, Quaternion.identity) as GameObject;
            Destroy(sparkGO, 1);
            Destroy(gameObject);

        }
    }



}
