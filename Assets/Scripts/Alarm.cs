﻿using UnityEngine;
using System.Collections;
using SunsetRiders;
using System.Collections.Generic;

public class Alarm : MonoBehaviour
{

    // BoxCollider2D collider;
    List<Transform> children;

    // Use this for initialization
    void Start()
    {
        children = new List<Transform>();
        //    collider = GetComponent<BoxCollider2D>();
        foreach (Transform child in transform)
        {
            children.Add(child);
            child.gameObject.SetActive(false);
        }
    }
	
    // Update is called once per frame
    void Update()
    {
	
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        Destroy(GetComponent<BoxCollider2D>());
        AudioManager.Instance.PlayAlarm();
        Debug.Log("alrarm");

        foreach (Transform child in transform)
        {
            children.Add(child);
            child.gameObject.SetActive(true);
        }

    }
}
