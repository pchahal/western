﻿using UnityEngine;
using System.Collections;
using SunsetRiders;
using System.Collections.Generic;

public class Player : Character
{
    private bool doubleJump = false;

    public void Awake()
    {
        base.Awake();       

      
        InputComponent = new KeyboardMouseInput();
    }

    public override void Shoot()
    {     
        AnimatorStateInfo stateinfo = anim.GetCurrentAnimatorStateInfo(0);
        if (InputComponent.FireButton)
        {
            anim.SetTrigger("Shoot");
            GameObject.Instantiate(SparkGO, GetMuzzlePosition(stateinfo, InputComponent.Direction, scale, MuzzleGO), Quaternion.identity);
            Transform bullet = GameObject.Instantiate(BulletGO, GetMuzzlePosition(stateinfo, InputComponent.Direction, scale, MuzzleGO), Quaternion.identity) as Transform;
            bullet.GetComponent<Bullet>().direction = GetGunAimVector(stateinfo, InputComponent.Direction, scale).normalized;
            bullet.GetComponent<Bullet>().speed = bulletForce;
            AudioManager.Instance.PlayShotGun();
        }
    }

    public void Update()
    {
        InputComponent.Update();      
        DrawDebug();
    }

    public void FixedUpdate()
    {        
        AnimatorStateInfo stateinfo = anim.GetCurrentAnimatorStateInfo(0);

        float jumpForce = 450;
        float radius = .3f;
        Collider2D collider = Physics2D.Raycast(GroundCheck.position, Vector2.down, radius, Utility.GetLayer("Ground") | Utility.GetLayer("Stairs")).collider;
        if (collider != null)
        {
            grounded = true;
            doubleJump = false;
            if (collider.tag == "Platform")
                anim.SetBool("OnPlatform", true);
            else
            {
                anim.SetBool("OnPlatform", false);
            }
        }
        else
        {
            grounded = false;
            anim.SetBool("OnPlatform", false);
        }

        if (grounded)
            Debug.DrawLine(GroundCheck.position, GroundCheck.position + Vector3.down * radius, Color.red);
        else
            Debug.DrawLine(GroundCheck.position, GroundCheck.position + Vector3.down * radius, Color.green);        

        anim.SetBool("Grounded", grounded);
        FaceDirection(stateinfo);
        anim.SetFloat("DirectionX", InputComponent.Direction.x);
        anim.SetFloat("DirectionY", InputComponent.Direction.y);
        anim.SetFloat("Speed", GetSpeed(stateinfo));

        CheckForJump(stateinfo, jumpForce);          
        CheckForLadder(stateinfo, InputComponent.Direction.y);
        Shoot();
        CheckForStairs(InputComponent.Direction.y > 0 ? true : false);
    }



    private float GetSpeed(AnimatorStateInfo stateInfo)
    {
        float speed = Mathf.Abs(InputComponent.Direction.x);
        if (stateInfo.IsName("Hanging") && speed > 0 && InputComponent.Direction.y != 0)
            return 0;
        else
        {
            return speed;
        }
    }

  



    private void CheckForJump(AnimatorStateInfo stateinfo, float jumpForce)
    {
        Rigidbody2D rigidBody = rigidBody2D;
        if (InputComponent.JumpButton)
        {
            if (stateinfo.IsName("Walking") && InputComponent.Direction.y < 0)
            {                   
                rigidBody.velocity = Vector2.right * 7;
                anim.SetBool("Sliding", true);

            }
            else if (stateinfo.IsName("Standing") || stateinfo.IsName("Walking") || stateinfo.IsName("Jumping"))
            {
                if (grounded || !doubleJump)
                {
                    anim.SetBool("Grounded", false);
                    anim.SetBool("OnRope", false);
                    rigidBody.velocity = new Vector2(rigidBody.velocity.x, 0);
                    if (!(stateinfo.IsName("Hanging") && InputComponent.Direction.y >= 0))
                        rigidBody.velocity = new Vector2(rigidBody.velocity.x, 7);
                    if (!doubleJump && !grounded)
                        doubleJump = true;
                }
            }
            else if (stateinfo.IsName("Hanging") || stateinfo.IsName("Shimmy"))
            {
                anim.SetBool("OnRope", false);
                if (InputComponent.Direction.y >= 0)
                    rigidBody.AddForce(Vector2.up * jumpForce);
                doubleJump = true;
            }
            else if (stateinfo.IsName("WallSlide"))
            {               

                anim.SetBool("OnWall", false);                                               
                Vector2 jumpDir = new Vector2(7, 7);
                rigidBody.velocity = new Vector2(jumpDir.x * scale, jumpDir.y);
                doubleJump = true;

            } 


        }

    }

  
    public Vector3 GetMuzzlePosition(AnimatorStateInfo stateinfo, Vector2 Velocity, float scale, List<Transform> muzzleGO)
    {
        if (stateinfo.IsName("Walking") || stateinfo.IsName("Jumping") || stateinfo.IsName("Hanging"))
        {
            if (Mathf.Abs(Velocity.x) < .1f && Velocity.y > 0)
                return muzzleGO[0].position;
            else if (Velocity.x > 0 && Velocity.y > 0)
                return muzzleGO[1].position;
            else if (Velocity.x > 0 && Mathf.Abs(Velocity.y) < .1f)
                return muzzleGO[2].position;
            else if (Velocity.x > 0 && Velocity.y < 0)
                return muzzleGO[3].position;
            else if (Mathf.Abs(Velocity.x) < .1f && Velocity.y < 0)
                return muzzleGO[4].position;
            else if (Velocity.x < 0 && Velocity.y < 0)
                return muzzleGO[3].position;
            else if (Velocity.x < 0 && Mathf.Abs(Velocity.y) < .1f)
                return muzzleGO[2].position;
            else if (Velocity.x < 0 && Velocity.y > 0)
                return muzzleGO[1].position;
        }
        else if (stateinfo.IsName("Ducking"))
            return muzzleGO[5].position;
        else if (stateinfo.IsName("Standing") && (Mathf.Abs(Velocity.x) < .1f && Velocity.y > 0))
            return muzzleGO[0].position;


        return muzzleGO[2].position;
    }

    public Vector2 GetGunAimVector(AnimatorStateInfo stateinfo, Vector2 Velocity, float scale)
    {

        if (stateinfo.IsName("Walking") || stateinfo.IsName("Jumping") || stateinfo.IsName("DuckingOnPlatform") || stateinfo.IsName("Hanging"))
        {
            if (Mathf.Abs(Velocity.x) < .1f && Velocity.y > 0)
                return Vector2.up;
            else if (Velocity.x > 0 && Velocity.y > 0)
                return new Vector2(1, 1);
            else if (Velocity.x > 0 && Mathf.Abs(Velocity.y) < .1f)
                return Vector2.right;
            else if (Velocity.x > 0 && Velocity.y < 0)
                return new Vector2(1, -1);
            else if (Mathf.Abs(Velocity.x) < .1f && Velocity.y < 0)
                return Vector2.down;
            else if (Velocity.x < 0 && Velocity.y < 0)
                return new Vector2(-1, -1);
            else if (Velocity.x < 0 && Mathf.Abs(Velocity.y) < .1f)
                return Vector2.left;
            else if (Velocity.x < 0 && Velocity.y > 0)
                return new Vector2(-1, 1);
        }
        else if (stateinfo.IsName("Standing") && (Mathf.Abs(Velocity.x) < .1f && Velocity.y > 0))
            return Vector2.up;


        return Vector2.right * scale;


    }

    public void FaceDirection(AnimatorStateInfo  stateinfo)
    {       

       
       
        if (InputComponent.Direction.x < 0)
            scale = -1;
        else if (InputComponent.Direction.x > 0)
            scale = 1;

        if (stateinfo.IsName("WallSlide") && InputComponent.JumpButton)
        {
            if (scale > 0)
                scale = -1;
            else
                scale = 1;            
        }


        transform.localScale = new Vector3(scale, 1, 1);    



    }
}
