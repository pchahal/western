﻿using UnityEngine;
using System.Collections;
using SunsetRiders;
using System.Collections.Generic;
using UnityEngine.Assertions;
using DG.Tweening;

public class Enemy : Character
{
    private Vector2 targetPos;

    [SerializeField]
    private List<Transform> wayPoint;

    public List<Transform> WayPoints
    { get { return wayPoint; } }


    public Vector2 TargetPos
    {
        get { return targetPos; }
        set
        {
            targetPos = value;
            if (targetPos.x < transform.position.x)
                transform.localScale = new Vector3(-1, 1, 1);
            else
                transform.localScale = new Vector3(1, 1, 1);
        }
    }

    public  void  Awake()
    {
        base.Awake();

        InputComponent = new AiInput();
        OnBecameInvisible();
     
       

       
    }

    void OnBecameVisible()
    {        
        

        GetComponent<AudioSource>().enabled = true;
        this.enabled = true;
    }

    void OnBecameInvisible()
    {
        GetComponent<AudioSource>().enabled = false;
        this.enabled = false;       
    }

    public void Update()
    {
        
        InputComponent.Update();                                   
        DrawDebug();
    }

    public void FixedUpdate()
    { 
        float radius = .3f;
        grounded = Physics2D.Raycast(GroundCheck.position, Vector2.down, radius, Utility.GetLayer("Ground") | Utility.GetLayer("Stairs")).collider;
        if (grounded)
            Debug.DrawLine(GroundCheck.position, GroundCheck.position + Vector3.down * radius, Color.red);
        else
            Debug.DrawLine(GroundCheck.position, GroundCheck.position + Vector3.down * radius, Color.green);

        FaceDirection();  


    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        
        if (collision.transform.tag == "Player")
        {
            collision.transform.GetComponent<IHittable>().OnHit(collision.contacts[0].point, InputComponent.Direction, 1);
            anim.SetBool("Reload", true);

        }

    }


    public void FaceDirection()
    {
        if (TargetPos != Vector2.zero)
        {
            //StandingDir
            if (TargetPos.x >= transform.position.x)
                scale = 1;
            else
                scale = -1;
        }
        
        //WalkingDir
        if (rigidBody2D.velocity.x < 0)
            scale = -1;
        if (rigidBody2D.velocity.x > 0)
            scale = 1;
        transform.localScale = new Vector3(scale, 1, 1);    
    }

  
    public override void Shoot()
    {
        bulletCount--;
        anim.SetTrigger("Shoot");
        GameObject.Instantiate(SparkGO, GetMuzzlePosition(), Quaternion.identity);
        Transform bullet = GameObject.Instantiate(BulletGO, GetMuzzlePosition(), Quaternion.identity) as Transform;
        bullet.GetComponent<Bullet>().speed = bulletForce;

        Vector2 targetDir = (TargetPos - new Vector2(transform.position.x, transform.position.y)).normalized;
        bullet.GetComponent<Bullet>().direction = GetGunAimVector(TargetPos).normalized;
        AudioManager.Instance.PlayShotGun();

    }

    public Vector3 GetMuzzlePosition()
    {
        return MuzzleGO[0].position;
    }

    public Vector2 GetGunAimVector(Vector3 targetPos)
    {
        Vector2 aimDir = (targetPos - transform.position).normalized;

        return aimDir;
    }

    public bool IsPlayerInSight()
    {

        if (TargetPos != Vector2.zero)
            return true;
        else
        {
            return false;
        }
    }



    public void MoveDownPosition()
    {
        transform.DOLocalMove(new Vector3(0, -1, 0), 1); 

    }

    void OnDrawGizmos()
    {
       


        /*EnemyPatrolling patrol = anim.GetBehaviour<EnemyPatrolling>();
        if (patrol != null)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(patrol.target, .5f);    
                
        }*/

    }
}
