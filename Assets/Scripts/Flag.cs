﻿using UnityEngine;
using System.Collections;
using SunsetRiders;
using UnityEngine.UI;

public class Flag : MonoBehaviour
{
    
    // Use this for initialization
    void Start()
    {
	
    }
	
    // Update is called once per frame
    void Update()
    {
	
    }


    public void OnTriggerEnter2D(Collider2D collider)
    {

        if (collider.tag == "Player")
        {
            AudioManager.Instance.PlayAlert();

            Invoke("LoadLevel", 3);

            ;
        }
    }

    void LoadLevel()
    {
        UnityEngine.Application.LoadLevel(UnityEngine.Application.loadedLevel + 1);        
    }
}
