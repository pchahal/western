﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class SmoothCamera2D : MonoBehaviour
{

    public float dampTime = 0.15f;
    private Vector3 velocity = Vector3.zero;
    public Transform target;
    private float orthoSize;
    private Camera cam;
    public Vector3 targetOffset;

    // Update is called once per frame
    void Start()
    {
        EventManager.Instance.AddListener(EVENT_TYPE.DEAD, OnEvent);
        EventManager.Instance.AddListener(EVENT_TYPE.CAMSHAKE, OnEvent);
        cam = GetComponent<Camera>();
        orthoSize = cam.orthographicSize;

    }

    void LateUpdate()
    {
       
        if (target)
        {
            Vector3 newTargetOffset;
            newTargetOffset = new Vector3(targetOffset.x * target.transform.localScale.x, targetOffset.y, targetOffset.z);

            Vector3 point = GetComponent<Camera>().WorldToViewportPoint(target.position);
            Vector3 delta = target.position + newTargetOffset - GetComponent<Camera>().ViewportToWorldPoint(new Vector3(0.5f, 0.5f, point.z)); //(new Vector3(0.5, 0.5, point.z));
            Vector3 destination = transform.position + delta;
            transform.position = Vector3.SmoothDamp(transform.position, destination, ref velocity, dampTime);
        }

    }

    void OnCamShake()
    {
        cam.DOShakePosition(2, 1);
    }


    public void OnEvent(EVENT_TYPE Event_Type, Component Sender, object Param = null)
    {
        //Detect event type
        switch (Event_Type)
        {
            case EVENT_TYPE.DEAD:
                OnPlayerDied();
                break;
            case EVENT_TYPE.CAMSHAKE:
                OnCamShake();
                break;
                
        }
    }
    //-------------------------------------------------------
    //Function called when health changes
    void OnPlayerDied()
    {
        
        Time.timeScale = .4f;
        cam.DOOrthoSize(3, 3).OnComplete(ResetCam);
      
    }

    void ResetCam()
    {
        //cam.orthographicSize = orthoSize;
        cam.DOOrthoSize(orthoSize, 3);
        Time.timeScale = 1;
     
    }

    void OnDrawGizmos()
    {

        // Debug.DrawLine(transform.position, Vector3.zero, Color.red);
            
    }

           
}