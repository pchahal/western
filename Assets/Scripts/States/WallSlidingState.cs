﻿using UnityEngine;
using System.Collections;
using SunsetRiders;

public class WallSlidingState : StateMachineBehaviour
{

    private Character player;
    private Rigidbody2D rigidBody;

    private float startTime;
    // OnStateEnter is called before OnStateEnter is called on any state inside this state machine
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        player = animator.GetComponent<Character>();
        rigidBody = player.rigidBody2D;
	
        startTime = Time.time;
        ;
    }

    //OnStateUpdate is called before OnStateUpdate is called on any state inside this state machine
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

        bool onWall = true;

        rigidBody.velocity = new Vector2(player.InputComponent.Direction.x * player.Speed, rigidBody.velocity.y);

        Vector3 origin = player.transform.position + new Vector3(0, -.5f, 0);
        RaycastHit2D hit = Physics2D.Raycast(origin, Vector2.right * player.scale, .6f, Utility.GetLayer("Ground"));
	
        if (hit.collider == null)
            onWall = false;
        else
        {
       
            // if (hit.collider.tag == "Wall")
            {
                rigidBody.velocity = new Vector2(rigidBody.velocity.x, -1);
            } 
            if (player.grounded == true)
                onWall = false;        
            if (Time.time - startTime > .1f)
            {
                startTime = Time.time;
                player.DrawDustPuff();
            }
        }            
        animator.SetBool("OnWall", onWall);

        if (onWall)
            Debug.DrawLine(origin, origin + Vector3.right * player.scale * .6f, Color.red);
        else
            Debug.DrawLine(origin, origin + Vector3.right * player.scale * .6f, Color.green);        
    }


}
