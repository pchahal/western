﻿using UnityEngine;
using System.Collections;
using SunsetRiders;

public class EnemyStanding : StateMachineBehaviour
{

    private Enemy enemy = null;
    private Rigidbody2D rigidbody;

    private float startTime;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        enemy = animator.gameObject.GetComponent<Enemy>();
        rigidbody = enemy.rigidBody2D;
        rigidbody.velocity = new Vector2(0, rigidbody.velocity.y);
        startTime = Time.time;
    }

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {        
        if (Time.time > startTime + enemy.fireRate)
        {
            if (enemy.TargetPos != Vector2.zero)
            {
                enemy.Shoot();
                startTime = Time.time;
            }
        }
        if (Time.time > startTime + 3 && enemy.WayPoints.Count > 0)
        {
            if (enemy.TargetPos == Vector2.zero)
            {
                animator.SetFloat("Speed", 1);

            }
        }
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
    }


    // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

}


