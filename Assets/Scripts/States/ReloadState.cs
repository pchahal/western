﻿using UnityEngine;
using System.Collections;
using SunsetRiders;

public class ReloadState : StateMachineBehaviour
{

    float startTime;
    private Character enemy;


    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        startTime = Time.time;
        enemy = animator.GetComponent<Character>();

    }



    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

        if (Time.time > startTime + enemy.reloadTime)
            animator.SetBool("Reload", false);
    }


    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        enemy.bulletCount = enemy.bulletCapacity;

        EnemyPatrolling patrol = animator.GetBehaviour<EnemyPatrolling>();
        if (patrol != null)
            patrol.currentWaypoint = patrol.path.vectorPath.Count;
    }

    // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {

    //
    //}
    // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}


}
