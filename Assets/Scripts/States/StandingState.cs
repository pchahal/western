﻿using UnityEngine;
using System.Collections;
using SunsetRiders;
using DG.Tweening;

public class StandingState : StateMachineBehaviour
{

    private Character player;


    // OnStateEnter is called before OnStateEnter is called on any state inside this state machine
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        player = animator.GetComponent<Character>();
    


    }

    //OnStateUpdate is called before OnStateUpdate is called on any state inside this state machine
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {



       


        if (player.CoverSpot != Vector3.zero && player.InputComponent.CoverButton == true)
        {

            animator.SetBool("OnCover", true);



        }

    }


}
