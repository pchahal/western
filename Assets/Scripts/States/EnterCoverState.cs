﻿using UnityEngine;
using System.Collections;
using SunsetRiders;

public class EnterCoverState : StateMachineBehaviour
{
    private Enemy enemy = null;


    private float startTime;

    public void test()
    {
    }

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        enemy = animator.gameObject.GetComponent<Enemy>();

        startTime = Time.time;
    }




    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

       
      

        if (Time.time > startTime + 2)
        {         
            if (enemy.TargetPos != Vector2.zero)
            {
                animator.SetFloat("Speed", 1);

            }
        }
    }





    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
  
    }

}
