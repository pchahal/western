﻿using UnityEngine;
using System.Collections;
using SunsetRiders;

public class JumpingState : StateMachineBehaviour
{

    private Character player;
    private Rigidbody2D rigidBody;
    private float originalGravity;
    // OnStateEnter is called before OnStateEnter is called on any state inside this state machine
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        player = animator.GetComponent<Character>();
        rigidBody = player.rigidBody2D;

       
    }

    //OnStateUpdate is called before OnStateUpdate is called on any state inside this state machine
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        bool onWall = false;
        float velx = Mathf.Clamp(rigidBody.velocity.x + player.InputComponent.Direction.x * player.Speed, -5, 5);
        rigidBody.velocity = new Vector2(velx, rigidBody.velocity.y);
        Vector3 origin = player.transform.position + new Vector3(0, -.5f, 0);
        RaycastHit2D hit = Physics2D.Raycast(origin, Vector2.right * player.scale, .6f, Utility.GetLayer("Ground"));

        if (hit.collider != null)
        {
            //  if (hit.collider.tag == "Wall")
            {
                
                onWall = true;
            } 

        }
    

        animator.SetBool("OnWall", onWall);

        if (onWall)
            Debug.DrawLine(origin, origin + Vector3.right * player.scale * .6f, Color.red);
        else
            Debug.DrawLine(origin, origin + Vector3.right * player.scale * .6f, Color.green);
        

    }
}