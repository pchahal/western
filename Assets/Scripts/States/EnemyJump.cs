﻿using UnityEngine;
using System.Collections;
using SunsetRiders;

public class EnemyJump : StateMachineBehaviour
{
    private Character enemy = null;
    private Rigidbody2D rigidbody;


    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        enemy = animator.gameObject.GetComponent<Character>();
        rigidbody = enemy.rigidBody2D;
		
    }


    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (enemy.grounded == true)
            animator.SetBool("Grounded", true);
        rigidbody.velocity = new Vector2(enemy.Speed, rigidbody.velocity.y);


    }


    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

    }

    // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}
}
