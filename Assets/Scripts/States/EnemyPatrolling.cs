﻿using UnityEngine;
using System.Collections;
using SunsetRiders;
using Pathfinding;

public class EnemyPatrolling : StateMachineBehaviour
{
    private Enemy enemy = null;
       
    private Seeker seeker;
    public Vector2 target;

    public Path path;

    public int currentWaypoint { get ; set; }

    public Vector3 prevDirection;
    public int prevWaypoint;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        enemy = animator.gameObject.GetComponent<Enemy>();
        seeker = enemy.GetComponent<Seeker>();    
        GetPath();
    }

    private void GetPath()
    {

        if (path != null)
        if (currentWaypoint < path.vectorPath.Count)
            return;
        //get new waypoint targetManager.cs to move to
        Vector2 prevTarget = target;

        if (enemy.IsPlayerInSight())
            target = enemy.TargetPos;
        else
            target = enemy.WayPoints[(Random.Range(0, enemy.WayPoints.Count))].position;
        while (target == prevTarget)
            target = enemy.WayPoints[(Random.Range(0, enemy.WayPoints.Count))].position;



        seeker.StartPath(enemy.GroundCheck.position, target, OnPathComplete);        
    }

    public void OnPathComplete(Path p)
    {        
        if (!p.error)
        {
            path = p;
            currentWaypoint = 0;
            prevWaypoint = 0;
            enemy.InputComponent.Direction = (path.vectorPath[currentWaypoint] - path.vectorPath[prevWaypoint]).normalized;
            prevDirection = enemy.InputComponent.Direction;
        }      
    }


    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

        if (path == null)
        {           
            return;
        }


      
        if ((currentWaypoint >= path.vectorPath.Count))
        {
            Debug.Log("End Of Path Reached");
            animator.SetFloat("Speed", 0);
            return;
        }
         

            
        DebugDrawNextWaypoint();
        enemy.scale = Mathf.Sign(enemy.InputComponent.Direction.x);
        enemy.rigidBody2D.velocity = new Vector2(enemy.scale * enemy.Speed, enemy.rigidBody2D.velocity.y);
        enemy.CheckForStairs(enemy.InputComponent.Direction.y > .25 ? true : false);       
        prevDirection = enemy.InputComponent.Direction;
        enemy.InputComponent.Direction = (path.vectorPath[currentWaypoint] - enemy.GroundCheck.position).normalized;

        if (Vector3.Dot(enemy.InputComponent.Direction, prevDirection) < .99f)
        {
            currentWaypoint++;        
            if (currentWaypoint < path.vectorPath.Count)
            {               
                Vector3 ladderDirection = path.vectorPath[currentWaypoint] - path.vectorPath[currentWaypoint - 1];
                enemy.InputComponent.Direction = (path.vectorPath[currentWaypoint] - enemy.GroundCheck.position).normalized;
                enemy.CheckForLadder(animator.GetCurrentAnimatorStateInfo(0), ladderDirection.y);
            }                
        }      

       
    }

   
    void DebugDrawNextWaypoint()
    {
        
        //  Debug.Log("current=" + currentWaypoint + " dist=" + Vector3.Distance(enemy.GroundCheck.position, path.vectorPath[currentWaypoint]));
        if (Vector3.Distance(enemy.GroundCheck.position, path.vectorPath[currentWaypoint]) < .2f)
            Debug.DrawLine(enemy.GroundCheck.position + new Vector3(0, .1f, 0), path.vectorPath[currentWaypoint], Color.red);
        else
            Debug.DrawLine(enemy.GroundCheck.position + new Vector3(0, .1f, 0), path.vectorPath[currentWaypoint], Color.yellow);    
        if (path != null)
        {
            for (int i = 1; i < path.vectorPath.Count; i++)
            {
                Debug.DrawLine(path.vectorPath[i - 1], path.vectorPath[i], Color.magenta);    
            }
        }

    }

    /*
    private bool IsJumpAvailable()
    {
        bool isJumpAvailable = false;
        LayerMask layer = 1 << 8;
        Vector3 origin = enemy.transform.position + Vector3.down * .5f;
        RaycastHit2D[] hit = Physics2D.RaycastAll(origin, Vector2.down, 1, layer);
        foreach (var c in hit)
        {
            if (c.collider != null)
            {
                if (c.collider.tag == "JumpPoint")
                    isJumpAvailable = true;
            }
        }
        if (isJumpAvailable)
            Debug.DrawLine(origin, origin + Vector3.down, Color.red);
        else
            Debug.DrawLine(origin, origin + Vector3.down, Color.white);
        return isJumpAvailable;
    }
*/


   
}
