﻿using UnityEngine;
using System.Collections;
using SunsetRiders;

public class EnemyClimbingState : StateMachineBehaviour
{

    private Character enemy;


    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        enemy = animator.GetComponent<Enemy>();

    }


    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

        enemy.transform.position += new Vector3(0, .1f * enemy.InputComponent.Direction.y, 0);
        enemy.CheckForLadder(animator.GetCurrentAnimatorStateInfo(0), enemy.InputComponent.Direction.y);

    }



}
