﻿using UnityEngine;
using System.Collections;
using SunsetRiders;
using DG.Tweening;

public class WalkingState : StateMachineBehaviour
{

    private Character player;
    private Rigidbody2D rigidBody;

    // OnStateEnter is called before OnStateEnter is called on any state inside this state machine
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        player = animator.GetComponent<Character>();
        rigidBody = player.rigidBody2D;


    }

    //OnStateUpdate is called before OnStateUpdate is called on any state inside this state machine
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {



        rigidBody.velocity = new Vector2(player.InputComponent.Direction.x * player.Speed, rigidBody.velocity.y);

      

        if (player.CoverSpot != Vector3.zero && player.InputComponent.CoverButton == true)
        {
            
            animator.SetBool("OnCover", true);


         
        }

    }

    // OnStateExit is called before OnStateExit is called on any state inside this state machine
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateMove is called before OnStateMove is called on any state inside this state machine
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateIK is called before OnStateIK is called on any state inside this state machine
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateMachineEnter is called when entering a statemachine via its Entry Node
    //override public void OnStateMachineEnter(Animator animator, int stateMachinePathHash){
    //
    //}

    // OnStateMachineExit is called when exiting a statemachine via its Exit Node
    override public void OnStateMachineExit(Animator animator, int stateMachinePathHash)
    {
        rigidBody.velocity = Vector3.zero;
    }
}
