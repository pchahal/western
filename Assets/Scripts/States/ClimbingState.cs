﻿using UnityEngine;
using System.Collections;
using SunsetRiders;

public class ClimbingState : StateMachineBehaviour
{

    private Character player;


    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        player = animator.GetComponent<Character>();
       
    }

   
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

        player.transform.position += new Vector3(.1f * player.InputComponent.Direction.x, .1f * player.InputComponent.Direction.y, 0);
      

    }

   

}
