﻿using UnityEngine;
using System.Collections;
using SunsetRiders;

public class ExitCoverState : StateMachineBehaviour
{
    private Enemy enemy = null;
    private Rigidbody2D rigidbody;

    private float startTime;
    Color debugColor = Color.clear;
   
    int shotsLeft = 0;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

        enemy = animator.gameObject.GetComponent<Enemy>();
        rigidbody = enemy.rigidBody2D;
        rigidbody.velocity = new Vector2(0, rigidbody.velocity.y);
        startTime = Time.time;

      

        if (shotsLeft == 0)
            shotsLeft = Random.Range(1, enemy.bulletCount);
        else
            shotsLeft--;
        

    }


    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

  

        if (enemy.bulletCount == 0)
            enemy.anim.SetBool("Reload", true);
        else if (enemy.TargetPos == Vector2.zero)
            animator.SetFloat("Speed", 0);
        else if (shotsLeft == 0)
            animator.SetFloat("Speed", 0);
        else if (Time.time > startTime + enemy.fireRate)
        {     
            if (enemy.TargetPos != Vector2.zero)
            {
                enemy.Shoot();
            }

        }




    }

  

}
