﻿using UnityEngine;
using System.Collections;
using SunsetRiders;

public class EnemyIdle : StateMachineBehaviour
{

    private Enemy enemy = null;
    private Rigidbody2D rigidbody;

    private float startTime;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        enemy = animator.gameObject.GetComponent<Enemy>();
        rigidbody = enemy.rigidBody2D;
        rigidbody.velocity = new Vector2(0, rigidbody.velocity.y);
        startTime = Time.time;
    }

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {   

        if (enemy.TargetPos != Vector2.zero)
        {
            animator.SetFloat("Speed", 1);
        }


        if (Time.time > startTime + 3)
        {
            animator.SetFloat("Speed", 1);
        }


    }


}


