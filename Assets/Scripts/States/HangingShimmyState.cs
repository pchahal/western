﻿using UnityEngine;
using System.Collections;
using SunsetRiders;

public class HangingShimmyState : StateMachineBehaviour
{

    private Character player = null;
    private Rigidbody2D rigidbody;
    private float originalGravity;


    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        player = animator.gameObject.GetComponent<Character>();
        rigidbody = player.rigidBody2D;
        originalGravity = rigidbody.gravityScale;
        rigidbody.gravityScale = 0f;
        rigidbody.velocity = Vector2.zero;

    }


    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (player.InputComponent.Direction.x >= 0)
            rigidbody.velocity = player.shimmyDir * player.Speed;
        else
        {
            rigidbody.velocity = player.shimmyDir * -1 * player.Speed;
        }

    }


    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {        
        rigidbody.gravityScale = originalGravity;

    }

    // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}
}
