﻿using UnityEngine;
using System.Collections;

public class WeakSpot : MonoBehaviour,IHittable
{

    public GameObject debris;

    public bool OnHit(Vector3 hitPoint, Vector2 hitDir, int amount)
    {
        
        Destroy(transform.parent.gameObject);
        Instantiate(debris, transform.position, Quaternion.identity);
        return true;    

    }

}
