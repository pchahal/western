﻿using UnityEngine;
using System.Collections;

public class Explosive : MonoBehaviour,IHittable
{

    public Transform explosionPrefab;
    public float TimeToExplode;
    public float radius = 3;
    private int instanceID;

    public void Start()
    {
        instanceID = gameObject.GetInstanceID();
        if (TimeToExplode > 0)
            Invoke("Explode", TimeToExplode);


    }

    public void Explode()
    {
        EventManager.Instance.PostNotification(EVENT_TYPE.CAMSHAKE, null, null);
        Destroy(gameObject);
        Instantiate(explosionPrefab, transform.position, Quaternion.identity);

        RaycastHit2D[] hits = Physics2D.CircleCastAll(transform.position, radius, Vector2.up, 1);
  
        foreach (var hit in hits)
        {
            IHittable hittable = hit.collider.GetComponent<IHittable>();
            if (hittable != null && hit.collider.gameObject.GetInstanceID() != instanceID)
            {         
                Vector3 hitDir = (hit.transform.position - transform.position).normalized;
                hittable.OnHit(hit.transform.position, hitDir, 1);        
            }
        

        }

    }

    public bool OnHit(Vector3 hitPoint, Vector2 hitDir, int amount)
    {
        CancelInvoke();
        Explode();

        return true;
       
    }

  
    void OnDrawGizmos()
    {
        Gizmos.color = new Color(1, 1, 0, .2f);
        Gizmos.DrawWireSphere(transform.position, radius);
    }
}
