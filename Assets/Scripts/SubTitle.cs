﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class SubTitle : MonoBehaviour
{
    
    public float elapsedTime;
    private int currentString;
    string[] subTitles;
    Text text;

    // Use this for initialization
    void Start()
    {
        currentString = 0;
   


        text = GetComponent<Text>();

        subTitles = text.text.Split('\n');

      

        text.text = subTitles[currentString];
        InvokeRepeating("ScrollText", elapsedTime, elapsedTime);
        Destroy(gameObject.transform.parent.gameObject, elapsedTime * (subTitles.Length + 1));
    }



    void ScrollText()
    {
      
        if (currentString < subTitles.Length - 1)
        {      
            currentString++;
            text.text = subTitles[currentString];        
        }
      



    }

}
