﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.ConstrainedExecution;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace SunsetRiders
{
    public class Character : MonoBehaviour, IHittable
    {
        public void Awake()
        {       
            
            Health = myscriptableObject.Health;
            Speed = myscriptableObject.Speed;
            reloadTime = myscriptableObject.reloadTime;
            fireRate = myscriptableObject.fireRate;
            lineOfSight = myscriptableObject.lineOfSight;
            bulletForce = myscriptableObject.bulletForce;
            bulletCount = myscriptableObject.bulletCount;
            bulletCapacity = myscriptableObject.bulletCapacity;

            scale = transform.localScale.x;
            anim = GetComponent<Animator>();
            rigidBody2D = GetComponent<Rigidbody2D>();

        }

        public virtual void  Shoot()
        {
            ;
        }

        public void CheckForStairs(bool ignore)
        {                     
            
            float radius = .3f;
            Collider2D ground = Physics2D.Raycast(GroundCheck.position, Vector2.down, radius, Utility.GetLayer("Ground")).collider;
            if (ground != null && ignore == false)
                ignore = true;
            else
                ignore = false;

            RaycastHit2D[] hits = Physics2D.BoxCastAll(transform.position, new Vector2(1, 2), 0, Vector2.right, .6f, Utility.GetLayer("Stairs"));
            if (hits.Length != 0)
                Debug.DrawLine(GroundCheck.position, GroundCheck.position + Vector3.right * scale * .25f, Color.red);
            else
                Debug.DrawLine(GroundCheck.position, GroundCheck.position + Vector3.right * scale * .25f, Color.green);


            foreach (var hit in hits)
            {                       
                if (hit.collider != null)
                {                                     
                    Physics2D.IgnoreCollision(hit.collider, GetComponent<CircleCollider2D>(), ignore);                     
                    Physics2D.IgnoreCollision(hit.collider, GetComponent<BoxCollider2D>(), ignore);    
                }
            }                        
        }

        public void CheckForLadder(AnimatorStateInfo stateInfo, float verticalDirection)
        {
            bool onLadder = false;

            if (stateInfo.IsName("ClimbingLadder"))
            {
                RaycastHit2D[] hits = Physics2D.RaycastAll(GroundCheck.position, Vector2.right, .25f, Utility.GetLayer("Doodads"));
                foreach (var hit in hits)
                {                       
                    if (hit.collider != null)
                    if (hit.collider.tag == "Ladder")
                        onLadder = true;                    
                }
                if (onLadder)
                    Debug.DrawLine(GroundCheck.position, GroundCheck.position + Vector3.right * .25f, Color.red, 0);
                else
                    Debug.DrawLine(GroundCheck.position, GroundCheck.position + Vector3.right * .25f, Color.white, 0);
            }
            else if (verticalDirection < 0)
            {
                RaycastHit2D[] hits = Physics2D.RaycastAll(GroundCheck.position + new Vector3(0, -.1f, 0), Vector2.down, .1f, Utility.GetLayer("Doodads"));
                foreach (var hit in hits)
                {                       
                    if (hit.collider != null)
                    if (hit.collider.tag == "Ladder")
                        onLadder = true;

                }
                if (onLadder)
                    Debug.DrawLine(GroundCheck.position, GroundCheck.position + Vector3.down * .1f, Color.red, 0);
                else
                    Debug.DrawLine(GroundCheck.position, GroundCheck.position + Vector3.down * .1f, Color.blue, 0);
            }
            else if (verticalDirection > 0)
            {
                RaycastHit2D[] hits = Physics2D.RaycastAll(GroundCheck.position + new Vector3(0, .1f, 0), Vector2.up, .1f, Utility.GetLayer("Doodads"));
                foreach (var hit in hits)
                {
                    if (hit.collider != null)
                    if (hit.collider.tag == "Ladder")
                        onLadder = true;                    
                }
                if (onLadder)
                    Debug.DrawLine(GroundCheck.position, GroundCheck.position + Vector3.up * .1f, Color.red, 0);
                else
                    Debug.DrawLine(GroundCheck.position, GroundCheck.position + Vector3.up * .1f, Color.blue, 0);
            }

            anim.SetBool("OnLadder", onLadder);




        }


        public bool OnHit(Vector3 hitPoint, Vector2 hitDir, int amount = 1)
        {
            if (!invincible)
            {
                Health -= amount;         
                Instantiate(blood[0], transform.position, Quaternion.identity);
                AudioManager.Instance.PlayGrunt();
                if (Health < 0)
                {                                           
                    anim.SetInteger("Health", Health);
                    rigidBody2D.velocity = new Vector2(hitDir.x, 1f) * 7;                        
                    if (tag == "Player")
                    {
                        EventManager.Instance.PostNotification(EVENT_TYPE.DEAD, this, transform.position);
                        AudioManager.Instance.PlayDie();
                        invincible = true;
                    }
                  
                    if (tag == "Enemy")
                    {
                        invincible = true;
                        EventManager.Instance.PostNotification(EVENT_TYPE.ENEMY_DEAD, this, transform.position);
                        //AudioManager.Instance.PlayDie();
                        //invincible = true;
                    }

                }
                return true;
            }
            else
                return false;
        }


        //called by player death animimation event
        private void Respawn()
        {         
            anim.SetInteger("Health", 10);
            transform.position += Vector3.up * 5;
            invincible = false;
        }

        //called by enemy death animation event
        public void Die()
        {            
            Destroy(gameObject);
            Debug.Log("die");
      
        }


        public void DrawDustPuff()
        {
            float xOffset = .5f;
            if (scale < 0)
                xOffset = -.5f; 
            Instantiate(DustPuffGO, transform.position + new Vector3(xOffset, 0, 0), Quaternion.identity);

        }

        protected void DrawDebug()
        {
            return;
            if (scale == -1)
                logText.GetComponentInParent<Transform>().localScale = new Vector3(-1, 1, 1);
            else
                logText.GetComponentInParent<Transform>().localScale = new Vector3(1, 1, 1);
            EnemyPatrolling patrol = anim.GetBehaviour<EnemyPatrolling>();

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Health " + Health);

            sb.AppendLine("Position " + transform.position);
            sb.AppendLine("Rigidbody.velocity " + rigidBody2D.velocity);
            sb.AppendLine("Direction " + InputComponent.Direction);

            if (patrol != null)
            {
                sb.AppendLine("prevDirection " + patrol.prevDirection);
                sb.AppendLine("CurrentWaypoint " + patrol.currentWaypoint);
            }
            logText.text = sb.ToString();
        }



        //***********************************************************************************************************
        public Transform GroundCheck { get { return groundCheck; } }

        [SerializeField]
        private Transform
            groundCheck;

        public List<Transform> MuzzleGO { get { return muzzleGO; } }

        [SerializeField]
        private List<Transform>
            muzzleGO;

        public Transform SparkGO { get { return sparkGO; } }

        [SerializeField]
        private Transform
            sparkGO;

        public Transform BulletGO { get { return bulletGO; } }

        [SerializeField]
        private Transform
            bulletGO;

        [SerializeField]
        private List<Transform>
            blood;

        public Transform DustPuffGO { get { return dustPuffGO; } }

        [SerializeField]
        private Transform
            dustPuffGO;

        public Transform ProjectileGO { get { return projectileGO; } }

        [SerializeField]
        private Transform
            projectileGO;

        public Text LogText { get { return logText; } }

        [SerializeField]
        private Text
            logText;

        [SerializeField]
        private PlayerScriptableObject myscriptableObject;

        public PlayerScriptableObject MyscriptableObject { get { return myscriptableObject; } }

        public bool invincible;

        public Animator anim { get; set; }

        public float scale { get; set; }

        public float reloadTime { get; set; }

        public float lineOfSight { get; set; }

        public float bulletForce { get; set; }

        public int bulletCount { get; set; }

        public int bulletCapacity { get; set; }

        public float fireRate { get; set; }

        public Vector2 shimmyDir { get; set; }

        public bool grounded { get; set; }

        public float Speed { get; set; }

        public Vector3 CoverSpot { get; set; }

        public Rigidbody2D rigidBody2D{ get; set; }

        public IInputComponent InputComponent { get; set; }

        protected int Health { get; set; }


    }

}
public interface IHittable
{
    bool OnHit(Vector3 hitPoint, Vector2 hitDir, int amount);
}
