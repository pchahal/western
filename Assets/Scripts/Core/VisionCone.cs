﻿using UnityEngine;
using System.Collections;
using SunsetRiders;

public class VisionCone : MonoBehaviour
{

    // Use this for initialization
    private Enemy player;
    public SpriteRenderer sprite;
    private float lastAlert;

    void Start()
    {
        player = transform.parent.GetComponent<Enemy>() as Enemy;
        lastAlert = Time.time;
    }

   

    public void OnTriggerEnter2D(Collider2D collider)
    {
        
        if (collider.tag == "Player")
        {
            if (collider is BoxCollider2D)
            {
                player.TargetPos = collider.transform.position;
                PlayAlert();
            }
        }


    }

   

    public void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.tag == "Player")
        {
            if (collider is BoxCollider2D)
            {
                player.TargetPos = Vector2.zero;
            
            }

        }

    }




    public void PlayAlert()
    {        

        if (Time.time - lastAlert > 7)
        {
            sprite.enabled = true;
            AudioManager.Instance.PlayAlert();
            Invoke("HideAlert", 2);
            lastAlert = Time.time;
        }
    }

    private void HideAlert()
    {
        sprite.enabled = false;
    }
}
