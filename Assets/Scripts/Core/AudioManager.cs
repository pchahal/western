﻿using System.Collections.Generic;
using UnityEngine;

namespace SunsetRiders
{
    public class AudioManager : Singleton<AudioManager>
    {

        [SerializeField]
        private List<AudioClip>
            audioClips;

        private AudioSource audioSource;


        void Awake()
        {
            audioSource = GetComponent<AudioSource>();
            Cursor.visible = false;
        }

        public void PlayShotGun()
        {
            //if (audioSource.isPlaying)
            //  audioSource.Stop();

            audioSource.PlayOneShot(audioClips[0]);

        }

        public void PlayExplode()
        {
            audioSource.PlayOneShot(audioClips[1]);

        }

        public void PlayCrateContact()
        {
            audioSource.PlayOneShot(audioClips[2]);

        }

        public void PlayCrateExplode()
        {
            audioSource.PlayOneShot(audioClips[3]);

        }

        public void PlayAlert()
        {
            audioSource.PlayOneShot(audioClips[4]);

        }

        public void PlayDie()
        {
            audioSource.PlayOneShot(audioClips[5]);

        }

        public void PlayDebris()
        {
            audioSource.PlayOneShot(audioClips[6]);

        }

        public void PlayGrunt()
        {
            audioSource.PlayOneShot(audioClips[7]);

        }

        public void PlayTrapDoor()
        {
            audioSource.PlayOneShot(audioClips[8]);

        }

        public void PlayAlarm()
        {
            audioSource.PlayOneShot(audioClips[9]);

        }

    }
}
