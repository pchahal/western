﻿using UnityEngine;

namespace SunsetRiders
{
	public  class HashId:MonoBehaviour
	{		 
		// Here we store the hash tags for various strings used in our animators.
		public static int Velocity;
		public static int PlayerFound;
		public static int PlayerLost;

		public static int Health;
		public static int Dead;
		public static int Shooting;

		void  Awake ()
		{
			Velocity = Animator.StringToHash ("Velocity");
			PlayerFound = Animator.StringToHash ("PlayerFound");
			PlayerLost = Animator.StringToHash ("PlayerLost");

			Health = Animator.StringToHash ("Health");
			Dead = Animator.StringToHash ("Dead");
			Shooting = Animator.StringToHash ("Shooting");
		}    
	}   



}


