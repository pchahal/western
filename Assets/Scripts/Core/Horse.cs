﻿using UnityEngine;
using System.Collections;

public class Horse : MonoBehaviour
{

    public Transform Cart;

    void Start()
    {
	
    }
	
    // Update is called once per frame
    void LateUpdate()
    {
        float newX = Cart.position.x + 3.5f;
        transform.position = new Vector3(newX, transform.position.y, transform.position.z);

    }
}
