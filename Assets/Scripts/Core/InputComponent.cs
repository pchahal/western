﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using UnityEngine;
using UnityEngine.UI;
using UnityEngineInternal;

namespace SunsetRiders
{
    class KeyboardMouseInput : IInputComponent
    {
        public void Update()
        {
            float hor = Input.GetAxis("Horizontal");
            float ver = Input.GetAxis("Vertical");
            Direction = new Vector2(hor, ver);

            JumpButton = false;
            if (Input.GetKeyDown(KeyCode.Space) || Input.GetButtonDown("Fire2"))
                JumpButton = true;

            FireButton = false;
            if (Input.GetKeyDown(KeyCode.N) || Input.GetButtonDown("Fire1"))
            {
                FireButton = true;
            }

            ThrowButtonDown = false;
            if (Input.GetKey(KeyCode.B) || Input.GetButton("Fire3"))
            {
                ThrowButtonDown = true;
            }
            ThrowButtonUp = false;
            if (Input.GetKeyUp(KeyCode.B) || Input.GetButtonUp("Fire3"))
            {
                ThrowButtonUp = true;
            }

            CoverButton = false;
            if (Input.GetKey(KeyCode.Z) || Input.GetButton("Fire4"))
            {
                CoverButton = true;
            }

         


        }



        public bool FireButton { get; set; }

        public bool ThrowButtonDown { get; set; }

        public bool ThrowButtonUp { get; set; }

        public bool JumpButton { get; set; }

        public Vector3 Direction  { get; set; }

        public bool CoverButton  { get; set; }
      
    }

    class AiInput : IInputComponent
    {
        public void Update()
        {
        }

        public bool FireButton { get; set; }

        public bool ThrowButtonDown { get; set; }

        public bool ThrowButtonUp { get; set; }

        public bool JumpButton { get; set; }

        public Vector3 Direction { get; set; }

        public bool CoverButton { get; set; }
    }


    class NullInput : IInputComponent
    {
        public void Update()
        {
        }

        public bool FireButton { get; set; }

        public bool ThrowButtonDown { get; set; }

        public bool ThrowButtonUp { get; set; }

        public bool JumpButton { get; set; }

        public Vector3 Direction { get; set; }

        public bool CoverButton { get; set; }
    }

    public interface IInputComponent
    {
        void Update();

        bool FireButton { get; set; }

        bool ThrowButtonDown { get; set; }

        bool ThrowButtonUp { get; set; }

        bool JumpButton { get; set; }

        bool CoverButton { get; set; }

        Vector3 Direction { get; set; }
    }
}
