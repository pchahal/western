﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using Application = UnityEngine.Application;
using SunsetRiders;


public class Rope : MonoBehaviour
{
    public Transform startPoint, endPoint;

    public LineRenderer lineRenderer;
    private Vector3 midPoint;

    public Vector2 RopeShimmyDir { get; set; }

    public Breakable window;


    public void Start()
    {
        UpdateRopePoints();
    }


   
    public void UpdateRopePoints()
    {

        lineRenderer.SetVertexCount(3);
        lineRenderer.SetPosition(0, startPoint.position);
        lineRenderer.SetPosition(2, endPoint.position);

        Vector3 midPoint = new Vector3(startPoint.position.x + (endPoint.position.x - startPoint.position.x) / 2.0f, startPoint.position.y + (endPoint.position.y - startPoint.position.y) / 2.0f, 0);
        lineRenderer.SetPosition(1, midPoint);
        GetRopeShimmyDir();
    }

    public void GetRopeShimmyDir()
    {
        Vector3 shimmyDir = new Vector3((endPoint.position.x - startPoint.position.x), (endPoint.position.y - startPoint.position.y)).normalized;
        RopeShimmyDir = shimmyDir;
    }

    public void SetRopeDip(Vector3 playerPos)
    {     
        float xPos = playerPos.x - startPoint.position.x;
        Vector2 offset = RopeShimmyDir * xPos;

        Vector3 ropeMidPoint = startPoint.position + new Vector3(offset.x, offset.y - .1f, -1);
        lineRenderer.SetPosition(1, ropeMidPoint);



    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "RopeHitBox")
        {
            Character player = collision.GetComponentInParent<Character>();
            player.shimmyDir = RopeShimmyDir;
            player.anim.SetBool("OnRope", true);
        }

    }

    public void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "RopeHitBox")
        {
            Character player = collision.GetComponentInParent<Character>();

            UpdateRopePoints();
            player.anim.SetBool("OnRope", false);
            if (window != null)
                window.OnHit(Vector3.zero, Vector2.zero, 0);
        }

    }

    public void OnTriggerStay2D(Collider2D collision)
    {		
        if (collision.tag == "RopeHitBox")
        {
            SetRopeDip(collision.transform.position);
        }
    }



}
