﻿using UnityEngine;
using System.Collections;
using SunsetRiders;

public class Projectile : MonoBehaviour
{
    LineRenderer line;
    private int numVertex;
    public Player player;
    public SpriteRenderer redTarget;
    private Vector3 forceDir;
    private float angle;




    public void Start()
    {
        line = GetComponent<LineRenderer>();
        angle = 45;       
    }


    public void Update()
    {        
        if (player.InputComponent.ThrowButtonDown)
        {
            // player.anim.SetBool("Throw", true);
            line.enabled = true;
            redTarget.enabled = true;           
            if (player.InputComponent.Direction.y > 0)
                angle += 1;
            else if (player.InputComponent.Direction.y < 0)
                angle -= 1;
            forceDir = Utility.AngleToVector(angle);
            forceDir = new Vector3(forceDir.x * player.scale, forceDir.y, forceDir.z) * 10;
            PlotTrajectory(transform.position, forceDir, .02f, 4);
        }
        else if (player.InputComponent.ThrowButtonUp)
        {
            player.anim.SetBool("Throw", false);
            ShootProjectile();
            line.enabled = false;
            redTarget.enabled = false;     
            angle = 45;
        }

    }

    public void ShootProjectile()
    {
        
       
       
        player.anim.SetTrigger("Shoot");
        GameObject.Instantiate(player.SparkGO, player.transform.position, Quaternion.identity);
        Transform grenade = GameObject.Instantiate(player.ProjectileGO, player.transform.position, Quaternion.identity) as Transform;
        grenade.GetComponent<Rigidbody2D>().AddForce(forceDir, ForceMode2D.Impulse);
        AudioManager.Instance.PlayShotGun();
    

    }


    public Vector3 PlotTrajectoryAtTime(Vector3 start, Vector3 startVelocity, float time)
    {
        return start + startVelocity * time + Physics.gravity * time * time * 0.5f;
    }

    public void PlotTrajectory(Vector3 start, Vector3 startVelocity, float timestep, float maxTime)
    {
        numVertex = 1;
        line.SetPosition(0, start);
        Vector3 prev = start;



        for (int i = 1;; i++)
        {
            float t = timestep * i;
            if (t > maxTime)
                break;
            Vector3 pos = PlotTrajectoryAtTime(start, startVelocity, t);
            if (Physics2D.Linecast(prev, pos, ~Utility.GetLayer("Player")))
            {
                redTarget.transform.position = pos;              
                break;            
            }
            // Debug.DrawLine(prev, pos, Color.red);

            numVertex++;
            line.SetVertexCount(numVertex);
            line.SetPosition(numVertex - 1, pos);


            prev = pos;
        }

    }

}
