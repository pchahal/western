﻿using UnityEngine;
using System.Collections;
using SunsetRiders;

public class LaserSight : MonoBehaviour
{
    LineRenderer line;
    public Enemy enemy;
    public SpriteRenderer redTarget;
    

    Vector3 startPoint;
    Vector2 endPoint;
    float deltaY;
    Collider hit = null;

    public void Start()
    {
        line = GetComponent<LineRenderer>();   

       
    }


    public void Update()
    {   





        startPoint = enemy.GetMuzzlePosition();
        deltaY = Mathf.PingPong(Time.time, 1) - .5f;
        //deltaY *= 2;
        endPoint = new Vector2(enemy.scale, deltaY);
        Ray2D ray = new Ray2D(startPoint, endPoint);
        endPoint = ray.GetPoint(50);

        line.SetPosition(0, startPoint);
        line.SetPosition(1, endPoint);

        RaycastHit2D hit = Physics2D.Linecast(startPoint, endPoint);
        if (hit.collider != null)
        {                     
            redTarget.enabled = true;
            redTarget.transform.position = hit.point;
            line.SetPosition(1, hit.point);
            if (hit.collider.tag == "Player")
            {
                enemy.TargetPos = hit.collider.transform.position;
                enemy.Shoot();
                enemy.TargetPos = Vector2.zero;

            }
        }
        else
            redTarget.enabled = false;        
    }
}
