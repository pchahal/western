﻿using UnityEngine;
using System.Collections;

public class Breakable    : MonoBehaviour,IHittable
{

    public Transform explosionPrefab;
    Collider2D collide;
    Animator anim;

    void Start()
    {
        collide = GetComponent<BoxCollider2D>();
        anim = GetComponent<Animator>();

    }

  

    public bool OnHit(Vector3 hitPoint, Vector2 hitDir, int amount)
    {
      
        Destroy(collide);


        Transform explosion = Instantiate(explosionPrefab, transform.position, Quaternion.identity) as Transform;
        float rot = 0;
        if (hitDir.x < 0)
            rot = 180;
        explosion.FindChild("Particles").rotation = Quaternion.Euler(new Vector3(0, 0, rot));
        Time.timeScale = .1f;
        Invoke("ResetTimeScale", .2f);
        anim.SetTrigger("Shoot");
      
        return true;
       
    }

  

    void ResetTimeScale()
    {        
        Time.timeScale = 1;
    }
  
}
