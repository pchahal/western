﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using SunsetRiders;

public class TrapDoor : MonoBehaviour
{
    public GameObject triggerGO;
    BoxCollider2D collider;

    void Start()
    {
        collider = GetComponent<BoxCollider2D>();
    }
	
    // Update is called once per frame
    void Update()
    {
	
    }

    void OnTriggerExit2D(Collider2D collder)
    {


        transform.DOLocalMoveY(transform.position.y - transform.localScale.y, 2);
        collider.enabled = false;
        AudioManager.Instance.PlayTrapDoor();
        Invoke("Trigger", 3);
    }

    void Trigger()
    {
        triggerGO.SetActive(true);
    }
}
