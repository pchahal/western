﻿using UnityEngine;
using System.Collections;
using SunsetRiders;

public class CoverSpot : MonoBehaviour
{
    void SetCover(Collider2D collider, Vector2 position)
    {
        if (collider.tag == "Player")
        {
            if (collider.GetComponent<BoxCollider2D>() != null)
            {
                Character player = collider.GetComponent<Character>() as Character;
                player.CoverSpot = position;
            }
        }
    }

    public void OnTriggerEnter2D(Collider2D collider)
    {                  
        SetCover(collider, transform.position);
    }

    public void OnTriggerStay2D(Collider2D collider)
    {                  
        SetCover(collider, transform.position);
    }


    public void OnTriggerExit2D(Collider2D collider)
    {
        SetCover(collider, Vector2.zero);
    }
}
