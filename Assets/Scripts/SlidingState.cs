﻿using UnityEngine;
using System.Collections;
using SunsetRiders;

public class SlidingState : StateMachineBehaviour
{

    private Character player;
    private float startTime;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        player = animator.GetComponent<Character>();
        float platformYPos = player.transform.position.y - 1;
        player.transform.position = new Vector3(player.transform.position.x, platformYPos, player.transform.position.z);
        startTime = Time.time;
    }





    //OnStateUpdate is called before OnStateUpdate is called on any state inside this state machine
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

        if (Time.time - startTime > .1f)
        {
            startTime = Time.time;
            player.DrawDustPuff();
        }
    }

    //OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        float platformYPos = player.transform.position.y + 1;
        player.transform.position = new Vector3(player.transform.position.x, platformYPos, player.transform.position.z);
    }




}
